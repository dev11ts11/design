<?php
/**
 * @category   Tecksky
 * @package    Tecksky_Banner
 * @author     Tecksky Team
 */
use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Tecksky_Design', __DIR__);

