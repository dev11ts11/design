<?php


namespace Tecksky\Design\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class AuthorizationViewModel implements ArgumentInterface
{

    protected $_customerSession;

    public function __construct(
        \Magento\Customer\Model\SessionFactory $customerSession
    )
    {
        $this->_customerSession = $customerSession;
    }

    public function getAccountLabel()
    {
        $customerSession = $this->_customerSession->create();

        if ($customerSession->isLoggedIn()) {
            $customerSession->getCustomerId();  // get Customer Id
            $customerSession->getCustomerGroupId();
            $customerSession->getCustomer();
            $customerSession->getCustomerData();

            return $customerSession->getCustomer()->getFirstname();  // get  Full Name
        }
        return __("My Account");
        
    }

    public function getAccountUrl()
    {
        return "customer/account";
    }
}