<?php


namespace Tecksky\Design\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class PriceViewModel implements ArgumentInterface
{
    protected $_priceCurrency;

    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
    )
    {
        $this->_priceCurrency = $priceCurrency;
    }

    public function getCurrencyWithFormat($price)
    {
        return $this->_priceCurrency->format($price,true,2);
    }
}