<?php


namespace Tecksky\Design\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class DateViewModel implements ArgumentInterface
{
    protected $_dateTimeFactory;

    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateTimeFactory
    )
    {
        $this->_dateTimeFactory = $dateTimeFactory;
    }

    public function getDateWithFormat($format = null,$date = null)
    {
        $dateModel = $this->_dateTimeFactory->create();
        return $dateModel->gmtDate($format,$date); // formate date using magento
    }

    public function getExpectedDispatchDate($afterDay = 0)
    {
        $time = time() + ($afterDay * 24 * 60 * 60); // Count date
        return $this->getDateWithFormat("l, F j", $time);//return formated date
    }
}