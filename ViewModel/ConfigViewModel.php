<?php


namespace Tecksky\Design\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class ConfigViewModel implements ArgumentInterface
{
    /**
     * @var ScopeInterface
     */
    private $scope;

    public function __construct(
        ScopeConfigInterface $scope
    )
    {
        $this->scope = $scope;
    }

    public function getStoreContactNumber()
    {
        return $this->scope->getValue('general/store_information/phone',ScopeInterface::SCOPE_STORES);
    }
    
    public function getStoreContactEmail()
    {
        return $this->scope->getValue('trans_email/ident_general/email');
    }
}